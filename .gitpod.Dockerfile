FROM gitpod/workspace-full

USER gitpod

RUN sudo apt-get update -q \
    && sudo apt-get install -y php-dev

RUN wget http://xdebug.org/files/xdebug-3.1.2.tgz \
    && tar -xvzf xdebug-3.1.2.tgz \
    && cd xdebug-3.1.2 \
    && phpize \
    && ./configure \
    && make \
    && sudo cp modules/xdebug.so /usr/lib/php/20200930 \
    && sudo bash -c "echo -e '\nzend_extension = /usr/lib/php/20200930/xdebug.so\n[XDebug]\nxdebug.remote_enable = 1\nxdebug.remote_autostart = 1\n' >> /etc/php/8.0/cli/php.ini"
